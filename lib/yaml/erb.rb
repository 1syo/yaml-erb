require "yaml/erb/version"
require "yaml"
require "erb"

module Psych
  module Erb
    def load_erb(content)
      Psych.load(::ERB.new(content).result)
    end

    def load_erb_file(filename)
      Psych.load(::ERB.new(File.read(filename)).result)
    end
  end

  extend Erb
end
