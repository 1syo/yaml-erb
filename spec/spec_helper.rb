require 'coveralls'
Coveralls.wear!

$:.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))

require 'pry'
require 'minitest/autorun'
require 'yaml/erb'
