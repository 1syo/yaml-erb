$:.unshift(File.join(File.dirname(__FILE__), '..', '..','spec'))
require "spec_helper"

describe Psych do
  describe ".load_erb" do
    let(:erb) do
      <<-ERB
        yaml:
          database: <%= "mysql" %>
          user: <%= "user" %>
        <% if false %>
          password: <%= "password" %>
        <% end %>
      ERB
    end

    it "has key" do
      Psych.load_erb(erb)["yaml"]["database"].must_equal "mysql"
    end

    it "not have key" do
      Psych.load_erb(erb)["yaml"].include?("password").must_equal false
    end
  end

  describe ".load_erb_file" do
    let(:file) { File.join(File.dirname(__FILE__), '..', 'fixture.yaml') }

    it "has key" do
      Psych.load_erb_file(file)["yaml"]["database"].must_equal "mysql"
    end

    it "not have key" do
      Psych.load_erb_file(file)["yaml"].include?("password").must_equal false
    end
  end
end
